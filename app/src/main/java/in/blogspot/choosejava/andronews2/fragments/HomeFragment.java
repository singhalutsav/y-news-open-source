package in.blogspot.choosejava.andronews2.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.HomeSectionAdapter;
import in.blogspot.choosejava.andronews2.misc.Utils;
import in.blogspot.choosejava.andronews2.models.NewsArticleCategoryModel;

public class HomeFragment extends Fragment {

    private HomePagerAdapter mHomePagerAdapter;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mHomePagerAdapter = new HomePagerAdapter(getActivity().getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) view.findViewById(R.id.fragment_home_container);
        mViewPager.setAdapter(mHomePagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.fragment_home_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        TabLayout.Tab firstTab = tabLayout.getTabAt(0);
        if(firstTab != null){
            firstTab.setIcon(R.drawable.ic_featured_new2);
        }

        return view;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FragmentTabHome extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_CATEGORY_STRING = "category_string";
        private static final String ARG_CATEGORY_INDEX = "category_index";

        private String category;
        private RecyclerView myRecyclerView;
        private ProgressBar myProgressBar;

        private SwipeRefreshLayout mySwipeRefreshLayout;

        public FragmentTabHome() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static FragmentTabHome newInstance(int sectionNumber) {
            String category = null;
            category = Utils.CATEGORIES[sectionNumber];
            FragmentTabHome fragment = new FragmentTabHome();
            Bundle args = new Bundle();
            args.putString(ARG_CATEGORY_STRING, category);
            args.putInt(ARG_CATEGORY_INDEX, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tab_home, container, false);
            int sectionNumber = -1;
            if(getArguments() != null){
                category = getArguments().getString(ARG_CATEGORY_STRING);
                sectionNumber = getArguments().getInt(ARG_CATEGORY_INDEX);
            }
            if(category == null)
                throw new RuntimeException("Category is null");

            if(category.equals("") && sectionNumber > 0)
                throw new RuntimeException("Same fragment bug");

            mySwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.fragment_tab_home_swipe_refresh);
            mySwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            // This method performs the actual data-refresh operation.
                            // The method calls setRefreshing(false) when it's finished.
                            updateDataSetRetroFit(mySwipeRefreshLayout);
                        }
                    }
            );

            //RecyclerView code
            myRecyclerView = (RecyclerView)rootView.findViewById(R.id.fragment_tab_home_recyclerView);
            myProgressBar = (ProgressBar)rootView.findViewById(R.id.fragment_tab_home_progressBar);
            HomeSectionAdapter adapter = new HomeSectionAdapter(new ArrayList<NewsArticleCategoryModel.ArticlesBean>(), category);
            myRecyclerView.setAdapter(adapter);
            updateDataSetRetroFit(null);
            return rootView;
        }

        private void updateDataSetRetroFit(SwipeRefreshLayout myView){
            Utils.fetchNewsArticles(category, getActivity(), myRecyclerView, myProgressBar, myView);
        }
    }

    public class HomePagerAdapter extends FragmentPagerAdapter {

        public HomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return FragmentTabHome.newInstance(position);
        }

        @Override
        public int getCount() {
            return Utils.CATEGORIES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Utils.CATEGORIES[position];
        }
    }
}
