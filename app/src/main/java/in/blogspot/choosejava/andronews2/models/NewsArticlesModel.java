package in.blogspot.choosejava.andronews2.models;

import java.util.List;

/**
 * Created by Admin on 10/5/2017.
 */

public class NewsArticlesModel {

    /**
     * status : ok
     * source : bbc-news
     * sortBy : top
     * articles : [{"author":"BBC News","title":"Vegas gunman's girlfriend 'had no clue'","description":"Marilou Danley said she had no idea her \"kind, caring, quiet\" partner was planning the mass killing.","url":"http://www.bbc.co.uk/news/world-us-canada-41504063","urlToImage":"https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/7A01/production/_98133213_mediaitem98133210.jpg","publishedAt":"2017-10-05T06:01:46Z"},{"author":"BBC News","title":"Iraq 'retakes' centre of IS-held Hawija","description":"Hawija district is one of the last two enclaves of so-called Islamic State in Iraq.","url":"http://www.bbc.co.uk/news/world-middle-east-41509085","urlToImage":"https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/BEF4/production/_98148884_iraq.jpg","publishedAt":"2017-10-05T07:25:29Z"},{"author":"BBC News","title":"How US mass shootings are getting worse","description":"Deadly rampages are happening more often and claiming more lives. Here's how the US changed.","url":"http://www.bbc.co.uk/news/av/world-us-canada-41474594/las-vegas-how-us-mass-shootings-are-getting-worse","urlToImage":"https://ichef.bbci.co.uk/news/1024/cpsprodpb/944C/production/_98146973_massacretimeline2_03469.jpg","publishedAt":"2017-10-04T22:10:10Z"},{"author":"BBC News","title":"Spain condemns independence 'blackmail'","description":"Catalan leaders are given a stern rebuke amid talk of declaring independence in days.","url":"http://www.bbc.co.uk/news/world-europe-41509050","urlToImage":"https://ichef.bbci.co.uk/news/1024/cpsprodpb/AB76/production/_98149834_042189756-1.jpg","publishedAt":"2017-10-05T06:55:54Z"},{"author":"BBC News","title":"Wreck could be sunken Athenia from WW2","description":"The remains of a passenger liner torpedoed by a U-boat just hours into the war may have been found.","url":"http://www.bbc.co.uk/news/science-environment-41503664","urlToImage":"https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/4C58/production/_98144591_wreck_no._111.jpg","publishedAt":"2017-10-04T23:13:54Z"},{"author":"BBC News","title":"Trump paper towel-throwing 'abominable'","description":"Puerto Rico mayor says the visiting president was \"insulting\" to the island's hurricane-hit people.","url":"http://www.bbc.co.uk/news/world-us-canada-41504165","urlToImage":"https://ichef.bbci.co.uk/news/1024/cpsprodpb/EAD1/production/_98131106_tv042165880.jpg","publishedAt":"2017-10-04T18:51:58Z"},{"author":"BBC News","title":"US and Niger soldiers 'killed in ambush'","description":"The attack near the Mali border comes as US forces help the Niger army fight Islamist militants.","url":"http://www.bbc.co.uk/news/world-africa-41507337","urlToImage":"https://ichef.bbci.co.uk/news/1024/cpsprodpb/7AF2/production/_98147413_nigermaliniamey9761017.jpg","publishedAt":"2017-10-05T07:53:07Z"},{"author":"BBC News","title":"Childhood bullying anxiety 'goes away'","description":"The researchers say this shows the potential for resilience in bullied children.","url":"http://www.bbc.co.uk/news/health-41503014","urlToImage":"https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/AD42/production/_98145344_c0357187-depressed_teenage_boy-spl-2.jpg","publishedAt":"2017-10-04T22:43:34Z"},{"author":"BBC News","title":"Indonesians eat defeated giant python","description":"Villagers ate the nearly 8m (26ft) long reptile in Sumatra after a local man wrestled and killed it.","url":"http://www.bbc.co.uk/news/world-asia-41507967","urlToImage":"https://ichef.bbci.co.uk/news/1024/cpsprodpb/A32E/production/_98147714_042178970-1.jpg","publishedAt":"2017-10-05T05:20:58Z"},{"author":"BBC News","title":"Tories rally round PM after speech woes","description":"MPs offer support after Theresa May's mishap-strewn speech but questions remain about her future.","url":"http://www.bbc.co.uk/news/uk-politics-41506740","urlToImage":"https://ichef.bbci.co.uk/images/ic/1024x576/p05j1vmd.jpg","publishedAt":"2017-10-05T08:09:27Z"}]
     */

    private String status;
    private String source;
    private String sortBy;
    private List<ArticlesBean> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public List<ArticlesBean> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticlesBean> articles) {
        this.articles = articles;
    }

    public static class ArticlesBean {
        /**
         * author : BBC News
         * title : Vegas gunman's girlfriend 'had no clue'
         * description : Marilou Danley said she had no idea her "kind, caring, quiet" partner was planning the mass killing.
         * url : http://www.bbc.co.uk/news/world-us-canada-41504063
         * urlToImage : https://ichef-1.bbci.co.uk/news/1024/cpsprodpb/7A01/production/_98133213_mediaitem98133210.jpg
         * publishedAt : 2017-10-05T06:01:46Z
         */

        private String author;
        private String title;
        private String description;
        private String url;
        private String urlToImage;
        private String publishedAt;

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public void setUrlToImage(String urlToImage) {
            this.urlToImage = urlToImage;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(String publishedAt) {
            this.publishedAt = publishedAt;
        }
    }
}
