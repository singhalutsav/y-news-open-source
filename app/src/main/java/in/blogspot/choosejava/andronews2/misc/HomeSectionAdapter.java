package in.blogspot.choosejava.andronews2.misc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.activities.NewsDetailsActivity;
import in.blogspot.choosejava.andronews2.models.NewsArticleCategoryModel;

public class HomeSectionAdapter extends RecyclerView.Adapter<HomeSectionAdapter.MyViewHolder> {

    private List<NewsArticleCategoryModel.ArticlesBean> articlesList;
    private String source;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public View mainView;
        public ImageView newsImageView;
        public TextView newsTitleTextView, newsAuthorTextView;
        public TextView newsDateTextView;

        public MyViewHolder(View view) {
            super(view);

            mainView = view;

            newsImageView = (ImageView) view.findViewById(R.id.home_tab_news_item_imageView);
            newsTitleTextView = (TextView) view.findViewById(R.id.home_tab_news_item_titleTextView);
            newsAuthorTextView = (TextView) view.findViewById(R.id.home_tab_news_item_authorTextView);
            newsDateTextView = (TextView) view.findViewById(R.id.home_tab_news_item_dateTextView);
            context = mainView.getContext();
        }
    }


    public HomeSectionAdapter(List<NewsArticleCategoryModel.ArticlesBean> articlesList, String source) {
        this.articlesList = articlesList;
        this.source = source;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_news_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NewsArticleCategoryModel.ArticlesBean article = articlesList.get(position);
        final String imageUrl = article.getUrlToImage();
        try {
            Picasso.with(holder.context)
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_loading_error)
                    .into(holder.newsImageView);
        } catch (RuntimeException exception){
            Picasso.with(holder.context)
                    .load(R.drawable.ic_loading_error)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_loading_error)
                    .into(holder.newsImageView);
        }String titleText = article.getTitle();
        holder.newsTitleTextView.setText(titleText);

        holder.newsAuthorTextView.setText(article.getAuthor());
        String publishedAt = article.getPublishedAt();
        if(publishedAt != null)
            if(!publishedAt.isEmpty())
                if(publishedAt.contains("T")){
                    publishedAt = publishedAt.substring(0, publishedAt.indexOf("T"));
                }
        holder.newsDateTextView.setText(publishedAt);

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = article.getUrl();
                Intent intent = new Intent(holder.context, NewsDetailsActivity.class);
                intent.putExtra(NewsDetailsActivity.URL_EXTRA, url);
                holder.context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articlesList.size();
    }

    public void updateDataset(List<NewsArticleCategoryModel.ArticlesBean> data){
        articlesList.clear();
        articlesList.addAll(data);
        notifyDataSetChanged();
    }
}