package in.blogspot.choosejava.andronews2.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import in.blogspot.choosejava.andronews2.BuildConfig;
import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.Utils;
import in.blogspot.choosejava.andronews2.misc.VideosSectionAdapter;
import in.blogspot.choosejava.andronews2.misc.VideosSectionAsyncTask;
import in.blogspot.choosejava.andronews2.models.YouTubeVideosModel;

public class VideosFragment extends Fragment {

    private VideosPagerAdapter mVideosPagerAdapter;
    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videos, container, false);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mVideosPagerAdapter = new VideosPagerAdapter(getActivity().getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) view.findViewById(R.id.fragment_videos_container);
        mViewPager.setAdapter(mVideosPagerAdapter);
        mViewPager.setOffscreenPageLimit(4);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.fragment_videos_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        TabLayout.Tab firstTab = tabLayout.getTabAt(0);
//        if(firstTab != null){
//            firstTab.setIcon(R.drawable.ic_featured_new2);
//            firstTab.setText("");
//        }

        return view;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FragmentTabVideos extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SEARCH_TERM_INDEX = "search_term_index";

        private String query;
        private RecyclerView myRecyclerView;
        private ProgressBar myProgressBar;

        private SwipeRefreshLayout mySwipeRefreshLayout;

        public FragmentTabVideos() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static FragmentTabVideos newInstance(int sectionNumber) {
            FragmentTabVideos fragment = new FragmentTabVideos();
            Bundle bundle = new Bundle();
            bundle.putInt(ARG_SEARCH_TERM_INDEX, sectionNumber);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tab_videos, container, false);

            mySwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.fragment_tab_videos_swipe_refresh);
            mySwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            // This method performs the actual data-refresh operation.
                            // The method calls setRefreshing(false) when it's finished.
                            updateDataSetRetroFit(mySwipeRefreshLayout);
                        }
                    }
            );
            //RecyclerView code
            int sectionNumber = -1;
            if(getArguments() != null){
                sectionNumber = getArguments().getInt(ARG_SEARCH_TERM_INDEX, -1);
                if(sectionNumber == -1)
                    throw new RuntimeException("Illegal section number: " + sectionNumber);
                query = Utils.VIDEO_SOURCES[sectionNumber][0];
            }

            myRecyclerView = (RecyclerView)rootView.findViewById(R.id.fragment_tab_video_recyclerView);
            myProgressBar = (ProgressBar)rootView.findViewById(R.id.fragment_tab_video_progressBar);
            VideosSectionAdapter adapter = new VideosSectionAdapter(new ArrayList<YouTubeVideosModel.ItemsBean>());
            myRecyclerView.setAdapter(adapter);
            updateDataSetRetroFit(null);
            return rootView;
        }

        private void updateDataSetRetroFit(SwipeRefreshLayout myView){
            Utils.fetchVideoData(query, getActivity(), myRecyclerView, myProgressBar, myView);
        }
    }

    public class VideosPagerAdapter extends FragmentPagerAdapter {

        public VideosPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return FragmentTabVideos.newInstance(position);
        }

        @Override
        public int getCount() {
            return Utils.VIDEO_SOURCES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            if(position == 0){
//                return Utils.FIREBASE_REMOTE_CONFIG.getString("videos_section_featured_heading");
//            }
//            return Utils.getCategories(getContext())[position - 1];
            return Utils.VIDEO_SOURCES[position][1];
        }
    }
}
