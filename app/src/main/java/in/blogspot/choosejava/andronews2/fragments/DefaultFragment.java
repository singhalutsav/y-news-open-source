package in.blogspot.choosejava.andronews2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.blogspot.choosejava.andronews2.R;

public class DefaultFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default, container, false);

        TextView textView = (TextView) view.findViewById(R.id.me_fragment_text_view);
        textView.setText("Coming Soon");
        return view;
    }
}
