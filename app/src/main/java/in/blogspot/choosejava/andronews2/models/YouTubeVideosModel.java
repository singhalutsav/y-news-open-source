package in.blogspot.choosejava.andronews2.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 10/10/2017.
 */

public class YouTubeVideosModel {

    /**
     * kind : youtube#searchListResponse
     * etag : "cbz3lIQ2N25AfwNr-BdxUVxJ_QY/6OcowUlcQjAmmljaMDN9D0RQM3E"
     * nextPageToken : CAUQAA
     * regionCode : IN
     * pageInfo : {"totalResults":779503,"resultsPerPage":5}
     * items : [{"kind":"youtube#searchResult","etag":"\"cbz3lIQ2N25AfwNr-BdxUVxJ_QY/fJjujZ2O3rNJKP0-zfakPhs7I5M\"","id":{"kind":"youtube#video","videoId":"wnA5VVB2wis"},"snippet":{"publishedAt":"2015-01-08T18:33:18.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"Zee Media Exclusive: Sudhir Chaudhary interviews MIM leader Asaduddin Owaisi","description":"Zee Media editor Sudhir Chaudhary talked exclusively to Akbaruddin Owaisi after his controversial remark that every child is born a Muslim.","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"cbz3lIQ2N25AfwNr-BdxUVxJ_QY/oYQRU1tcyWX15KInY4LduJ8-Q6I\"","id":{"kind":"youtube#video","videoId":"yUTBIlIyU4k"},"snippet":{"publishedAt":"2015-11-24T18:21:56.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"DNA: Reality check on how Muslims are treated around the world","description":"Watch a reality check on how Muslims are treated around the world and how no country is better than India for Muslims.","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/yUTBIlIyU4k/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/yUTBIlIyU4k/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/yUTBIlIyU4k/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"cbz3lIQ2N25AfwNr-BdxUVxJ_QY/uYqzrtDHBf5hSSqcxMiIRl_jU7w\"","id":{"kind":"youtube#video","videoId":"0KLOZ0rSiuI"},"snippet":{"publishedAt":"2017-06-30T17:13:28.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"DNA : What is #GST? | DNA में #GST पर सरल भाषा में संपूर्ण ज्ञान","description":"At midnight PM Narendra Modi will unveil the new tax regime replacing overnight the messy mix of more than a dozen state and central levies built up over ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/0KLOZ0rSiuI/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/0KLOZ0rSiuI/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/0KLOZ0rSiuI/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"cbz3lIQ2N25AfwNr-BdxUVxJ_QY/OFwlZqh7XLUJEUf0TSH79lj5Mfc\"","id":{"kind":"youtube#video","videoId":"e0wwSCHcxcM"},"snippet":{"publishedAt":"2016-08-03T18:02:52.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"DNA: Analysis of functioning of GST bill","description":"Analysis of new tax system GST and its benefits to the common man in India. Watch complete news story of DNA for getting detailed updates! Zee News always ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/e0wwSCHcxcM/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/e0wwSCHcxcM/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/e0wwSCHcxcM/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}},{"kind":"youtube#searchResult","etag":"\"cbz3lIQ2N25AfwNr-BdxUVxJ_QY/XLK-o0UMqH6VIMoQIx6qy5C5O0Y\"","id":{"kind":"youtube#video","videoId":"tgls4wTD-Js"},"snippet":{"publishedAt":"2016-10-28T18:01:47.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"DNA: Analysis of the most valuable and hidden currency 'Bitcoin'","description":"Analysis of the most valuable and hidden currency 'Bitcoin'. Watch this special segment and get to know more here. Zee News always stay ahead in bringing ...","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/tgls4wTD-Js/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/tgls4wTD-Js/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/tgls4wTD-Js/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}}]
     */

    private String kind;
    private String etag;
    private String nextPageToken;
    private String regionCode;
    private PageInfoBean pageInfo;
    private List<ItemsBean> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public PageInfoBean getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfoBean pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class PageInfoBean {
        /**
         * totalResults : 779503
         * resultsPerPage : 5
         */

        private int totalResults;
        private int resultsPerPage;

        public int getTotalResults() {
            return totalResults;
        }

        public void setTotalResults(int totalResults) {
            this.totalResults = totalResults;
        }

        public int getResultsPerPage() {
            return resultsPerPage;
        }

        public void setResultsPerPage(int resultsPerPage) {
            this.resultsPerPage = resultsPerPage;
        }
    }

    public static class ItemsBean {
        /**
         * kind : youtube#searchResult
         * etag : "cbz3lIQ2N25AfwNr-BdxUVxJ_QY/fJjujZ2O3rNJKP0-zfakPhs7I5M"
         * id : {"kind":"youtube#video","videoId":"wnA5VVB2wis"}
         * snippet : {"publishedAt":"2015-01-08T18:33:18.000Z","channelId":"UCIvaYmXn910QMdemBG3v1pQ","title":"Zee Media Exclusive: Sudhir Chaudhary interviews MIM leader Asaduddin Owaisi","description":"Zee Media editor Sudhir Chaudhary talked exclusively to Akbaruddin Owaisi after his controversial remark that every child is born a Muslim.","thumbnails":{"default":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/hqdefault.jpg","width":480,"height":360}},"channelTitle":"Zee News","liveBroadcastContent":"none"}
         */

        private String kind;
        private String etag;
        private IdBean id;
        private SnippetBean snippet;

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public String getEtag() {
            return etag;
        }

        public void setEtag(String etag) {
            this.etag = etag;
        }

        public IdBean getId() {
            return id;
        }

        public void setId(IdBean id) {
            this.id = id;
        }

        public SnippetBean getSnippet() {
            return snippet;
        }

        public void setSnippet(SnippetBean snippet) {
            this.snippet = snippet;
        }

        public static class IdBean {
            /**
             * kind : youtube#video
             * videoId : wnA5VVB2wis
             */

            private String kind;
            private String videoId;

            public String getKind() {
                return kind;
            }

            public void setKind(String kind) {
                this.kind = kind;
            }

            public String getVideoId() {
                return videoId;
            }

            public void setVideoId(String videoId) {
                this.videoId = videoId;
            }
        }

        public static class SnippetBean {
            /**
             * publishedAt : 2015-01-08T18:33:18.000Z
             * channelId : UCIvaYmXn910QMdemBG3v1pQ
             * title : Zee Media Exclusive: Sudhir Chaudhary interviews MIM leader Asaduddin Owaisi
             * description : Zee Media editor Sudhir Chaudhary talked exclusively to Akbaruddin Owaisi after his controversial remark that every child is born a Muslim.
             * thumbnails : {"default":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/default.jpg","width":120,"height":90},"medium":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/mqdefault.jpg","width":320,"height":180},"high":{"url":"https://i.ytimg.com/vi/wnA5VVB2wis/hqdefault.jpg","width":480,"height":360}}
             * channelTitle : Zee News
             * liveBroadcastContent : none
             */

            private String publishedAt;
            private String channelId;
            private String title;
            private String description;
            private ThumbnailsBean thumbnails;
            private String channelTitle;
            private String liveBroadcastContent;

            public String getPublishedAt() {
                return publishedAt;
            }

            public void setPublishedAt(String publishedAt) {
                this.publishedAt = publishedAt;
            }

            public String getChannelId() {
                return channelId;
            }

            public void setChannelId(String channelId) {
                this.channelId = channelId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public ThumbnailsBean getThumbnails() {
                return thumbnails;
            }

            public void setThumbnails(ThumbnailsBean thumbnails) {
                this.thumbnails = thumbnails;
            }

            public String getChannelTitle() {
                return channelTitle;
            }

            public void setChannelTitle(String channelTitle) {
                this.channelTitle = channelTitle;
            }

            public String getLiveBroadcastContent() {
                return liveBroadcastContent;
            }

            public void setLiveBroadcastContent(String liveBroadcastContent) {
                this.liveBroadcastContent = liveBroadcastContent;
            }

            public static class ThumbnailsBean {
                /**
                 * default : {"url":"https://i.ytimg.com/vi/wnA5VVB2wis/default.jpg","width":120,"height":90}
                 * medium : {"url":"https://i.ytimg.com/vi/wnA5VVB2wis/mqdefault.jpg","width":320,"height":180}
                 * high : {"url":"https://i.ytimg.com/vi/wnA5VVB2wis/hqdefault.jpg","width":480,"height":360}
                 */

                @SerializedName("default")
                private DefaultBean defaultX;
                private MediumBean medium;
                private HighBean high;

                public DefaultBean getDefaultX() {
                    return defaultX;
                }

                public void setDefaultX(DefaultBean defaultX) {
                    this.defaultX = defaultX;
                }

                public MediumBean getMedium() {
                    return medium;
                }

                public void setMedium(MediumBean medium) {
                    this.medium = medium;
                }

                public HighBean getHigh() {
                    return high;
                }

                public void setHigh(HighBean high) {
                    this.high = high;
                }

                public static class DefaultBean {
                    /**
                     * url : https://i.ytimg.com/vi/wnA5VVB2wis/default.jpg
                     * width : 120
                     * height : 90
                     */

                    private String url;
                    private int width;
                    private int height;

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }
                }

                public static class MediumBean {
                    /**
                     * url : https://i.ytimg.com/vi/wnA5VVB2wis/mqdefault.jpg
                     * width : 320
                     * height : 180
                     */

                    private String url;
                    private int width;
                    private int height;

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }
                }

                public static class HighBean {
                    /**
                     * url : https://i.ytimg.com/vi/wnA5VVB2wis/hqdefault.jpg
                     * width : 480
                     * height : 360
                     */

                    private String url;
                    private int width;
                    private int height;

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }
                }
            }
        }
    }
}
