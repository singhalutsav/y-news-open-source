package in.blogspot.choosejava.andronews2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.Utils;

public class FollowFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow_container, container, false);

        //RecyclerView code
        RecyclerView myRecyclerView = (RecyclerView)view.findViewById(R.id.fragment_follow_recyclerView);
        myRecyclerView.setAdapter(new FollowSectionAdapter());
        myRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        myRecyclerView.setHasFixedSize(true);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fragment_follow_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Coming soon", Toast.LENGTH_SHORT).show();
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });
        return view;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private static class FollowSectionAdapter extends RecyclerView.Adapter<FollowSectionAdapter.MyViewHolder> {

        private String[] categories = new String[]{
                "Business",
                "Entertainment",
                "General",
                "Health",
                "Science",
                "Sports",
                "Technology"
        };

        private int[] imageIds = new int[]{
                R.drawable.category_business,
                R.drawable.category_entertainment,
                R.drawable.category_general,
                R.drawable.category_health,
                R.drawable.category_science,
                R.drawable.category_sports,
                R.drawable.category_technology
        };

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Context context;
            public View mainView;
            public ImageView categoryImageView;
            public TextView categoryTextView;

            public MyViewHolder(View view) {
                super(view);

                mainView = view;

                categoryImageView = (ImageView) view.findViewById(R.id.follow_category_item_imageView);
                categoryTextView = (TextView) view.findViewById(R.id.follow_category_item_categoryTextView);
                context = mainView.getContext();
            }
        }

        @Override
        public FollowSectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.follow_category_item, parent, false);

            return new FollowSectionAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final FollowSectionAdapter.MyViewHolder holder, int position) {
            final int imageId = imageIds[position];
            Picasso.with(holder.context)
                    .load(imageId)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_loading_error)
                    .into(holder.categoryImageView);
            String categoryText = categories[position];
            holder.categoryTextView.setText(categoryText);
        }

        @Override
        public int getItemCount() {
            return imageIds.length;
        }
    }
}
