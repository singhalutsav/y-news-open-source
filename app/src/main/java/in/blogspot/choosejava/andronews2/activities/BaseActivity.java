package in.blogspot.choosejava.andronews2.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.blogspot.choosejava.andronews2.misc.NetworkStateReceiver;
import in.blogspot.choosejava.andronews2.misc.Utils;

public abstract class BaseActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!Utils.isNetworkAvailable(this)){
            Intent intent = new Intent(this, NetworkUnavailableActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void networkAvailable() {
        //Toast.makeText(this, "Network available", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void networkUnavailable() {
        //Toast.makeText(this, "Network unavailable", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, NetworkUnavailableActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
