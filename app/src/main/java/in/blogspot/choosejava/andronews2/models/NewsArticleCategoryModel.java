package in.blogspot.choosejava.andronews2.models;

import java.util.List;

/**
 * Created by utsavsinghal on 20/02/18.
 */

public class NewsArticleCategoryModel {

    /**
     * status : ok
     * totalResults : 20
     * articles : [{"source":{"id":null,"name":"Livemint.com"},"author":"Ian King","title":"Qualcomm increases offer for NXP, cuts deal with holders","description":"Qualcomm raised its offer for NXP Semiconductors NV and said it cut a deal with holders, in a bid to close a transaction that may be crucial to its own efforts to fend off a hostile approach by Broadcom","url":"http://www.livemint.com/Companies/nJ49MX5ti8gkVcAKQRcqkK/Qualcomm-increases-offer-for-NXP-cuts-deal-with-holders.html","urlToImage":"http://www.livemint.com/rf/Image-621x414/LiveMint/Period2/2018/02/21/Photos/Processed/qualcomm.jpg","publishedAt":"2018-02-20T14:49:14Z"},{"source":{"id":null,"name":"Livemint.com"},"author":"PTI","title":"Rotomac bank fraud: IT dept attaches 14 bank accounts","description":"The provisional attachment of Rotomac\u2019s bank accounts has been carried out to \u2018recover outstanding tax demands\u2019, understood to be about Rs85 crore","url":"http://www.livemint.com/Industry/dImPO18THKlLsV8w4c2CHO/Rotomac-bank-fraud-IT-dept-attaches-14-bank-accounts.html","urlToImage":"http://www.livemint.com/rf/Image-621x414/LiveMint/Period2/2018/02/21/Photos/Processed/vikramkotharinew-kCnD--621x414@LiveMint.jpg","publishedAt":"2018-02-20T14:43:43Z"},{"source":{"id":null,"name":"News18.com"},"author":"Aradhna Wal","title":"Delhi Hospitals Make Profits of 1737% on Masks, Gloves; Prescribe Expensive Branded Drugs: NPPA","description":"According to the government price regulatory body, the profit margin that private hospitals have on consumables range between 344 percent and 1737 percent. On non-scheduled drugs, it ranges between 158 percent and 1192 percent.","url":"http://www.news18.com/news/india/delhi-hospitals-make-profits-of-1737-on-masks-gloves-prescribe-costlier-drugs-nppa-1666901.html","urlToImage":"http://img01.ibnlive.in/ibnlive/uploads/2016/09/hospital_image.jpg","publishedAt":"2018-02-20T14:40:34Z"},{"source":{"id":null,"name":"Business-standard.com"},"author":"BS Web Team","title":"Jaitley breaks silence on PNB fraud, says India will chase down cheaters","description":"Nirav Modi wrote to banks were unable to clear dues because of the action taken in \"haste\" by PNB. Also, asked banks to pay salaries to 2,200 employees working in those firms. Know more on Nirav Modi letter.","url":"http://www.business-standard.com/article/finance/pnb-fraud-nirav-modi-blames-bank-s-haste-for-fiasco-top-10-developments-118022000081_1.html","urlToImage":"http://bsmedia.business-standard.com/_media/bs/img/article/2018-02/11/full/1518353521-6226.JPG","publishedAt":"2018-02-20T14:37:30Z"},{"source":{"id":null,"name":"Ndtv.com"},"author":null,"title":"Rupee Falls Sharply To Fresh 3-Month Low Against US Dollar","description":"On Friday, the rupee had ended 30 paise lower at 64.21 against the US dollar. The currency markets were closed on Monday.","url":"https://www.ndtv.com/business/dollar-rupee-exchange-rate-inr-falls-to-3-month-low-against-usd-1814814","urlToImage":"https://i.ndtvimg.com/i/2017-03/500-rupees-bill_650x400_61490342858.jpg","publishedAt":"2018-02-20T14:20:00Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":"PTI","title":"Bangladesh's DSE approves China's bid; rejects India's offer","description":"Shanghai and Shenzhen stock exchanges are among the top bourses in the world boasting $3.5 trillion and $ 2.2 trillion market capital, respectively.","url":"https://economictimes.indiatimes.com/markets/stocks/news/bangladeshs-dse-approves-chinas-bid-rejects-indias-offer/articleshow/63001376.cms","urlToImage":"https://img.etimg.com/thumb/msid-63001370,width-672,resizemode-4,imgsize-87621/finance-think.jpg","publishedAt":"2018-02-20T14:20:00Z"},{"source":{"id":null,"name":"Ndtv.com"},"author":null,"title":"Government Clears Opening Up Of Commercial Coal Mining To Private Firms","description":"In a major reform in the coal sector since its nationalisation in 1973, the government today allowed private companies to mine the fossil fuel for commercial use, ending the monopoly of state-owned Coal India Ltd (CIL).","url":"https://www.ndtv.com/india-news/government-clears-opening-up-of-commercial-coal-mining-to-private-firms-1815065","urlToImage":"https://i.ndtvimg.com/i/2018-01/piyush-goyal-davos_650x400_81516715951.jpg","publishedAt":"2018-02-20T14:07:00Z"},{"source":{"id":"the-guardian-au","name":"The Guardian (AU)"},"author":"Matthew Weaver","title":"KFC was warned about switching UK delivery contractor, union says","description":"GMB union says it expressed doubts about DHL\u2019s ability to run operation from single warehouse","url":"https://www.theguardian.com/business/2018/feb/20/kfc-was-warned-about-switching-uk-delivery-contractor-union-says","urlToImage":"https://i.guim.co.uk/img/media/4c4cb9b7710283862dea48fa7ec2ba8cbf4c1f42/8_0_5478_3286/master/5478.jpg?w=1200&h=630&q=55&auto=format&usm=12&fit=crop&crop=faces%2Centropy&bm=normal&ba=bottom%2Cleft&blend64=aHR0cHM6Ly91cGxvYWRzLmd1aW0uY28udWsvMjAxOC8wMS8zMS9mYWNlYm9va19kZWZhdWx0LnBuZw&s=28b6945950bc523fb13501cf370c8406","publishedAt":"2018-02-20T13:53:00Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":null,"title":"Railways announces 195 Trade Apprentice jobs, apply at rcf.indianrailways.gov.in (before March 19)","description":"Education News: NEW DELHI: Indian Railways Recruitment board has released a notification for the recruitment of 195 Trade Apprentice posts that has been issued by Rai","url":"https://timesofindia.indiatimes.com/home/education/news/railways-announces-195-trade-apprentice-jobs-apply-at-rcf-indianrailways-gov-in-before-march-19/articleshow/63000072.cms","urlToImage":"https://static.toiimg.com/photo/msid-63000099/63000099.jpg?94280","publishedAt":"2018-02-20T12:35:00Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":"Prashant Mahesh","title":"Sundaram MF suspended from BSE Star MF","description":"Sundaram Mutual Fund has been suspended from BSE StAR MF, a mutual fund portal of BSE, used by many investors to transact in mutual fund units.","url":"https://economictimes.indiatimes.com/mf/mf-news/sundaram-mf-suspended-from-bse-star-mf/articleshow/62999392.cms","urlToImage":"https://img.etimg.com/thumb/msid-62999428,width-672,resizemode-4,imgsize-20277/mutual-funds-_-ts.jpg","publishedAt":"2018-02-20T12:01:27Z"},{"source":{"id":null,"name":"Business-standard.com"},"author":"T E Narasimhan","title":"Chit Funds sore about amendment bill, say it won't safeguard public money","description":"The Rs 500 billion chit fund industry has expressed its disappointment with the Chit Funds (Amendment) Bill, 2018, cleared by the Union Cabinet today to act upon the unregulated deposit schemes. The industry has alleged that the Bill in its current","url":"http://www.business-standard.com/article/economy-policy/chit-funds-sore-about-amendment-bill-say-it-won-t-safeguard-public-money-118022000685_1.html","urlToImage":"http://bsmedia.business-standard.com/_media/bs/img/article/2015-11/03/full/1446572957-4705.jpg","publishedAt":"2018-02-20T11:48:23Z"},{"source":{"id":null,"name":"Moneycontrol.com"},"author":null,"title":"HDFC disbursed total subsidy worth Rs 302 crore under PMAY-CLSS","description":"As many as 14,290 beneficiaries benefited from the PMAY scheme across the country","url":"http://www.moneycontrol.com/news/business/real-estate/hdfc-disbursed-total-subsidy-worth-rs-302-crore-under-pmay-clss-2512053.html","urlToImage":"https://static-news.moneycontrol.com/static-mcnews/2017/12/hdfc-762x435.jpg","publishedAt":"2018-02-20T11:44:52Z"},{"source":{"id":null,"name":"Businesstoday.in"},"author":null,"title":"Mahindra Xylo, Verito, Vibe, Nuvosport to be discontinued by 2020","description":"This is Mahindra's preparation for the upcoming Bharat Stage VII norms and new crash safety standards. The BS-VI norms will be rolled out across India on April 1, 2020, whereas the new crash safety norms will be implemented from July 1, 2019.","url":"https://www.businesstoday.in/sectors/auto/mahindra-and-mahindra-xylo-verito-vibe-nuvosport-discontinued-by-2020/story/271075.html","urlToImage":"https://smedia2.intoday.in/btmt/images/stories/mahindra-505_022018043639.jpg","publishedAt":"2018-02-20T11:10:00Z"},{"source":{"id":null,"name":"Business-standard.com"},"author":"Dev Chatterjee","title":"ArcelorMittal, Numetal bids for Essar Steel fail eligibility test","description":"Matter could land in higher courts, as stakes for both Mittal and Ruias are high; bankers to call for fresh bids","url":"http://www.business-standard.com/article/companies/arcelormittal-numetal-bids-for-essar-steel-fail-eligibility-test-118022000705_1.html","urlToImage":"http://bsmedia.business-standard.com/_media/bs/img/article/2017-05/26/full/1495808351-8854.jpg","publishedAt":"2018-02-20T11:06:35Z"},{"source":{"id":null,"name":"Indiainfoline.com"},"author":"India Infoline News Service","title":"Uflex outlines Rs1,700cr investment in UP towards capacity expansion and in-house power generation","description":"The company will use Rs500cr for enhancing capacity and Rs1,200cr for solar power generation","url":"https://www.indiainfoline.com/article/news-top-story/uflex-outlines-rs1-700cr-investment-in-up-towards-capacity-expansion-and-in-house-power-generation-118022000404_1.html","urlToImage":"https://content.indiainfoline.com/_media/iifl/img/article/2017-01/09/full/1483950022-5006.jpg","publishedAt":"2018-02-20T11:00:31Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":"ETMarkets.com","title":"Ambuja Cements net profit swells 89% on demand pick-up","description":"Net sales for October-December jumped to Rs 2,612 crore, compared with Rs 2,188 crore in the same quarter last year.","url":"https://economictimes.indiatimes.com/markets/stocks/news/ambuja-cements-net-profit-swells-89-on-demand-pick-up/articleshow/62997681.cms","urlToImage":"https://img.etimg.com/thumb/msid-62997678,width-672,resizemode-4,imgsize-66937/rise-thinkstock.jpg","publishedAt":"2018-02-20T09:58:00Z"},{"source":{"id":null,"name":"Business-standard.com"},"author":"Alnoor Peermohamed","title":"Flipkart investment: How Walmart hopes to hit two birds with one stone","description":"The company will not only take on Amazon but also get a partner to enter India's offline retail space","url":"http://www.business-standard.com/article/companies/walmart-looks-to-hit-two-birds-with-one-stone-through-flipkart-investment-118022000536_1.html","urlToImage":"http://bsmedia.business-standard.com/_media/bs/img/article/2018-02/16/full/1518805523-2154.jpg","publishedAt":"2018-02-20T09:10:14Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":"ET Retail","title":"Customers of about 90 utility firms can pay via Google Tez","description":"Customers of about 90 utility firms including power, telecom and DTH, can now pay their bills online using Google Tez applications, the company said today.","url":"https://retail.economictimes.indiatimes.com/news/e-commerce/e-tailing/customers-of-about-90-utility-firms-can-pay-via-google-tez/62996850","urlToImage":"https://etimg.etb2bimg.com/thumb/msid-62996850,width-700,resizemode-4/customers-of-about-90-utility-firms-can-pay-via-google-tez.jpg","publishedAt":"2018-02-20T08:54:57Z"},{"source":{"id":null,"name":"Businesstoday.in"},"author":null,"title":"Pilot who built aircraft on rooftop signs Rs 35K-crore pact with Maha to make 1st plane factory of India","description":"The project, which also includes several ancillary hubs that'll be required in the facility, will be spread over 157 acre of land in Palghar district, which is around 140km from Mumbai.","url":"https://www.businesstoday.in/sectors/aviation/pilot-amol-yadav-aircraft-rooftop-signs-rs-35k-crore-pact-with-maha-1st-plane-factory/story/271065.html","urlToImage":"https://smedia2.intoday.in/btmt/images/stories/amol-yadav_505_022018021301.jpg","publishedAt":"2018-02-20T08:45:41Z"},{"source":{"id":"the-times-of-india","name":"The Times of India"},"author":"ET Retail","title":"Industry veterans stand in support of Paytm's appeal for fair play in payments","description":"Executive Director, Seedfund, Shailesh Vikram Singh echoed Ray's opinion and urged the government to look into the matter.","url":"https://retail.economictimes.indiatimes.com/news/e-commerce/e-tailing/industry-veterans-stand-in-support-of-paytms-appeal-for-fair-play-in-payments/62987158","urlToImage":"https://etimg.etb2bimg.com/thumb/msid-62987158,width-700,resizemode-4/industry-veterans-stand-in-support-of-paytm-s-appeal-for-fair-play-in-payments.jpg","publishedAt":"2018-02-20T02:21:03Z"}]
     */

    private String status;
    private int totalResults;
    private List<ArticlesBean> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<ArticlesBean> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticlesBean> articles) {
        this.articles = articles;
    }

    public static class ArticlesBean {
        /**
         * source : {"id":null,"name":"Livemint.com"}
         * author : Ian King
         * title : Qualcomm increases offer for NXP, cuts deal with holders
         * description : Qualcomm raised its offer for NXP Semiconductors NV and said it cut a deal with holders, in a bid to close a transaction that may be crucial to its own efforts to fend off a hostile approach by Broadcom
         * url : http://www.livemint.com/Companies/nJ49MX5ti8gkVcAKQRcqkK/Qualcomm-increases-offer-for-NXP-cuts-deal-with-holders.html
         * urlToImage : http://www.livemint.com/rf/Image-621x414/LiveMint/Period2/2018/02/21/Photos/Processed/qualcomm.jpg
         * publishedAt : 2018-02-20T14:49:14Z
         */

        private SourceBean source;
        private String author;
        private String title;
        private String description;
        private String url;
        private String urlToImage;
        private String publishedAt;

        public SourceBean getSource() {
            return source;
        }

        public void setSource(SourceBean source) {
            this.source = source;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public void setUrlToImage(String urlToImage) {
            this.urlToImage = urlToImage;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(String publishedAt) {
            this.publishedAt = publishedAt;
        }

        public static class SourceBean {
            /**
             * id : null
             * name : Livemint.com
             */

            private Object id;
            private String name;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
