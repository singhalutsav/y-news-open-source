package in.blogspot.choosejava.andronews2.misc;

import in.blogspot.choosejava.andronews2.models.NewsArticlesModel;
import in.blogspot.choosejava.andronews2.models.YouTubeVideosModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Admin on 10/22/2017.
 */

public interface VideosApi {
    @GET("/youtube/v3/search?part=snippet&type=video&maxResults=20&order=relevance")
    Call<YouTubeVideosModel> getVideosData
            (@Query("q") String searchTerm, @Query("key") String apiKey);
}
