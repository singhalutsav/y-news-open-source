package in.blogspot.choosejava.andronews2.misc;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import in.blogspot.choosejava.andronews2.models.YouTubeVideosModel;

/**
 * Created by Utsav on 12/2/2016.
 */
public class VideosSectionAsyncTask extends AsyncTask<String, String, String> {

    public static final String TAG = VideosSectionAsyncTask.class.getSimpleName();

    private ProgressBar progressBar;
    private Context context;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public VideosSectionAsyncTask(Context context, RecyclerView recyclerView, ProgressBar progressBar){
        this.context = context;
        this.recyclerView = recyclerView;
        this.progressBar = progressBar;
    }

    public VideosSectionAsyncTask(Context context, RecyclerView recyclerView,
                                  ProgressBar progressBar, SwipeRefreshLayout swipeRefreshLayout){
        this.context = context;
        this.recyclerView = recyclerView;
        this.progressBar = progressBar;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    protected void onPreExecute() {
        if(swipeRefreshLayout == null)
            progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... urls) {
        HttpURLConnection urlConnection;
        StringBuilder builder = new StringBuilder();
        try {
            /* forming th java.net.URL object */
            URL url = new URL(urls[0]);
            Log.v(TAG, "" + url);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            int statusCode = urlConnection.getResponseCode();
                /* 200 represents HTTP OK */
            if (statusCode == 200) {
                Log.v(TAG, "Response code is 200 OK");
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                    builder.append(inputLine);
                }
                in.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v(TAG, builder.toString());
        return builder.toString();
    }


    protected void onPostExecute(String result) {
        if(swipeRefreshLayout == null)
            progressBar.setVisibility(View.GONE);
        else swipeRefreshLayout.setRefreshing(false);

        try{
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(result));
            reader.setLenient(true);
            YouTubeVideosModel response = gson.fromJson(result, YouTubeVideosModel.class);
            List<YouTubeVideosModel.ItemsBean> items = response.getItems();
            ((VideosSectionAdapter)recyclerView.getAdapter()).updateDataset(items);
        } catch(RuntimeException exception){
            Toast.makeText(context, "Network not available!", Toast.LENGTH_SHORT).show();
        }
    }
}