package in.blogspot.choosejava.andronews2.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.PreferenceManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            public void run() {
                PreferenceManager preferenceManager = new PreferenceManager(SplashActivity.this);
                Intent intent = null;
                if(preferenceManager.isFirstTimeLaunch()){
                    preferenceManager.setFirstTimeLaunch(false);
                    intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                } else intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);

    }
}
