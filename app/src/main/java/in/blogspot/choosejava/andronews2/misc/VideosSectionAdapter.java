package in.blogspot.choosejava.andronews2.misc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.activities.VideoPlayerActivity;
import in.blogspot.choosejava.andronews2.models.YouTubeVideosModel;

public class VideosSectionAdapter extends RecyclerView.Adapter<VideosSectionAdapter.MyViewHolder> {

    private List<YouTubeVideosModel.ItemsBean> videosList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public View mainView;
        public ImageView videoThumbnailImageView;
        public TextView videoTitleTextView, videoDurationTextView, videoDateTextView;

        public MyViewHolder(View view) {
            super(view);

            mainView = view;

            videoThumbnailImageView = (ImageView) view.findViewById(R.id.video_tab_item_imageView);
            videoTitleTextView = (TextView) view.findViewById(R.id.video_tab_item_titleTextView);
            videoDurationTextView = (TextView) view.findViewById(R.id.video_tab_item_duration_textView);
            videoDateTextView = (TextView) view.findViewById(R.id.video_tab_item_date_textView);
            context = mainView.getContext();
        }
    }


    public VideosSectionAdapter(List<YouTubeVideosModel.ItemsBean> videosList) {
        this.videosList = videosList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.youtube_video_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final YouTubeVideosModel.ItemsBean video = videosList.get(position);

        final String imageUrl = video.getSnippet().getThumbnails().getMedium().getUrl();
        Picasso.with(holder.context)
                .load(imageUrl)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.ic_loading_error)
                .into(holder.videoThumbnailImageView);

        String titleText = video.getSnippet().getTitle();
        holder.videoTitleTextView.setText(titleText);

        String dateText = video.getSnippet().getPublishedAt();
        holder.videoDateTextView.setText(dateText);

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String videoId = video.getId().getVideoId();
                Intent intent = new Intent(holder.context, VideoPlayerActivity.class);
                intent.putExtra(VideoPlayerActivity.VIDEO_ID, videoId);
                holder.context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videosList.size();
    }

    public void updateDataset(List<YouTubeVideosModel.ItemsBean> data){
        videosList.clear();
        videosList.addAll(data);
        notifyDataSetChanged();
    }
}