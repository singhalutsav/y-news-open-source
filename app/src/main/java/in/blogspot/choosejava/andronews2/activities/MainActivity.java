package in.blogspot.choosejava.andronews2.activities;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.fragments.FollowFragment;
import in.blogspot.choosejava.andronews2.fragments.HomeFragment;
import in.blogspot.choosejava.andronews2.fragments.LoginFragment;
import in.blogspot.choosejava.andronews2.fragments.VideosFragment;
import in.blogspot.choosejava.andronews2.misc.CustomViewPager;
import in.blogspot.choosejava.andronews2.misc.Utils;

/**
 * @author Waleed Sarwar
 * @since March 30, 2016 11:54 AM
 */
public class MainActivity extends BaseActivity {
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup the viewPager
        CustomViewPager viewPager = (CustomViewPager) findViewById(R.id.main_view_pager);
        MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        if (viewPager != null) {
            viewPager.setPagingEnabled(false);
            viewPager.setAdapter(pagerAdapter);
            viewPager.setOffscreenPageLimit(3);
        }

        mTabLayout = (TabLayout) findViewById(R.id.main_tab_layout);
        if (mTabLayout != null) {
            mTabLayout.setupWithViewPager(viewPager);

            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }

            mTabLayout.getTabAt(0).getCustomView().setSelected(true);
        }

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "in.blogspot.choosejava.andronews2",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        final Task<Void> fetch = Utils.FIREBASE_REMOTE_CONFIG.fetch(Utils.EXPIRATION_TIME);
        fetch.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                Toast.makeText(MainActivity.this, "Values retrieved", Toast.LENGTH_SHORT).show();
                Utils.FIREBASE_REMOTE_CONFIG.activateFetched();
            }
        });
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {

        private final String[] mTabsTitle = new String[]{
            "Home", "Videos", "Follow", "Me"
        };

        private final int[] mTabsIcons = {
                R.drawable.ic_home_selector,
                R.drawable.ic_video_selector,
                R.drawable.ic_follow_selector,
                R.drawable.ic_me_selector
        };

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_tab, null);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(mTabsTitle[position]);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos){
                case 0: return new HomeFragment();
                case 1: return new VideosFragment();
                case 2: return new FollowFragment();
                case 3: return new LoginFragment();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return mTabsTitle.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

}
