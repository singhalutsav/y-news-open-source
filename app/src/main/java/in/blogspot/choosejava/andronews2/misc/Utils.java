package in.blogspot.choosejava.andronews2.misc;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import in.blogspot.choosejava.andronews2.BuildConfig;
import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.models.NewsArticleCategoryModel;
import in.blogspot.choosejava.andronews2.models.NewsArticlesModel;
import in.blogspot.choosejava.andronews2.models.YouTubeVideosModel;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Admin on 7/13/2017.
 */

public class Utils {
    public static final String HOME_SECTION_URL_OLD =
            "https://newsapi.org/v1/articles?source=%s&apiKey=%s";

    public static final String HOME_SECTION_URL =
            "https://newsapi.org/v2/top-headlines?category=%s&country=in&apiKey=%s";

    public static final String[] CATEGORIES = new String[]{
            "",
            "Business",
            "Entertainment",
            "General",
            "Health",
            "Science",
            "Sports",
            "Technology"
    };

    public static final String[][] VIDEO_SOURCES = new String[][]{
            {"LIVE NEWS INDIA", "LIVE TV"},
            {"DNA ZEE NEWS LATEST", "DNA ZEE NEWS"},
            {"TAAL THOK KE ZEE NEWS", "TAAL THOK KE"},
            {"AAJ KI BAAT LATEST", "AAJ KI BAAT"},
            {"ARNAB GOSWAMI LATEST DEBATE", "ARNAB GOSWAMI"}
    };

    public static final String[][] SECTION_LIST_OLD = new String[][]{
            {"bbc-news", "BBC News"}, {"bloomberg", "Bloomberg"},
            {"cnn", "CNN"},
            {"four-four-two", "FourFourTwo"}, {"fortune", "Fortune Magazine"},
            {"google-news", "Google News"},
            {"national-geographic", "National Geographic"}, {"new-scientist", "New Scientist"},
            {"techcrunch", "Techcrunch"}, {"the-economist", "The Economist"}
    };

    public static final String YOUTUBE_API_URL =
            "https://www.googleapis.com/youtube/v3/search?part=snippet&order=viewCount&q=%s&type=video&key=%s";

    public static NewsApi NEWS_API_INSTANCE = null;
    public static VideosApi VIDEOS_API_INSTANCE = null;

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null;
    }

    public static void startTimer(final ProgressBar progressBar){
        new CountDownTimer(4000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                progressBar.setVisibility(View.GONE);
            }

        }.start();
    }

    public static void initVideosApiRetrofit(){
        String baseUrl = "https://www.googleapis.com";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient.build())
                .build();

        VIDEOS_API_INSTANCE = retrofit.create(VideosApi.class);
    }

    public static void initNewsApiRetrofit(){
        String baseUrl = "https://newsapi.org";

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.client(httpClient.build())
                .build();

        NEWS_API_INSTANCE = retrofit.create(NewsApi.class);
    }

    public static void fetchNewsArticlesOld(String source, final Context context, final RecyclerView recyclerView,
                                         final ProgressBar progressBar, final SwipeRefreshLayout swipeRefreshLayout){
        if(swipeRefreshLayout == null)
            progressBar.setVisibility(View.VISIBLE);

        if(NEWS_API_INSTANCE == null)
            initNewsApiRetrofit();
        final List<NewsArticlesModel.ArticlesBean> result = new ArrayList<>();
        Call<NewsArticlesModel> call =
                NEWS_API_INSTANCE.getNewsArticles(source, BuildConfig.NEWS_API_KEY);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<NewsArticlesModel>() {
            @Override
            public void onResponse(Call<NewsArticlesModel> call, Response<NewsArticlesModel> response) {
                // The network call was a success and we got a response
                if(response.isSuccessful()) {
                    if(swipeRefreshLayout == null)
                        progressBar.setVisibility(View.GONE);
                    else swipeRefreshLayout.setRefreshing(false);
                    result.addAll(response.body().getArticles());
                    ((HomeSectionAdapterOld)recyclerView.getAdapter()).updateDataset(result);
                } else{
                    Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<NewsArticlesModel> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
                if(swipeRefreshLayout == null)
                    progressBar.setVisibility(View.GONE);
                else swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "In onFailure: " + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void fetchNewsArticles(String category, final Context context, final RecyclerView recyclerView,
                                         final ProgressBar progressBar, final SwipeRefreshLayout swipeRefreshLayout){
        if(swipeRefreshLayout == null)
            progressBar.setVisibility(View.VISIBLE);

        if(NEWS_API_INSTANCE == null)
            initNewsApiRetrofit();
        final List<NewsArticleCategoryModel.ArticlesBean> result = new ArrayList<>();
        Call<NewsArticleCategoryModel> call =
                NEWS_API_INSTANCE.getNewsArticles2(category, "in", BuildConfig.NEWS_API_KEY);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<NewsArticleCategoryModel>() {
            @Override
            public void onResponse(Call<NewsArticleCategoryModel> call, Response<NewsArticleCategoryModel> response) {
                // The network call was a success and we got a response
                if(response.isSuccessful()) {
                    if(swipeRefreshLayout == null)
                        progressBar.setVisibility(View.GONE);
                    else swipeRefreshLayout.setRefreshing(false);
                    result.addAll(response.body().getArticles());
                    ((HomeSectionAdapter)recyclerView.getAdapter()).updateDataset(result);
                } else{
                    Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<NewsArticleCategoryModel> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
                if(swipeRefreshLayout == null)
                    progressBar.setVisibility(View.GONE);
                else swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "In onFailure: " + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void fetchVideoData(String searchTerm, final Context context, final RecyclerView recyclerView,
                                         final ProgressBar progressBar, final SwipeRefreshLayout swipeRefreshLayout){
        if(swipeRefreshLayout == null)
            progressBar.setVisibility(View.VISIBLE);

        if(VIDEOS_API_INSTANCE == null)
            initVideosApiRetrofit();
        final List<YouTubeVideosModel.ItemsBean> result = new ArrayList<>();
        Call<YouTubeVideosModel> call =
                VIDEOS_API_INSTANCE.getVideosData(searchTerm, BuildConfig.YOUTUBE_API_KEY);

        // Execute the call asynchronously. Get a positive or negative callback.
        call.enqueue(new Callback<YouTubeVideosModel>() {
            @Override
            public void onResponse(Call<YouTubeVideosModel> call, Response<YouTubeVideosModel> response) {
                // The network call was a success and we got a response
                if(response.isSuccessful()) {
                    if(swipeRefreshLayout == null)
                        progressBar.setVisibility(View.GONE);
                    else swipeRefreshLayout.setRefreshing(false);
                    result.addAll(response.body().getItems());
                    ((VideosSectionAdapter)recyclerView.getAdapter()).updateDataset(result);
                } else{
                    Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<YouTubeVideosModel> call, Throwable t) {
                // the network call was a failure
                // TODO: handle error
                if(swipeRefreshLayout == null)
                    progressBar.setVisibility(View.GONE);
                else swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(context, "In onFailure: " + t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static String[] getCategories(Context context){
        PreferenceManager preferenceManager = new PreferenceManager(context);
        return preferenceManager.getCategoriesArray();
    }

    public static void setCategories(String[] categories, Context context){
        PreferenceManager preferenceManager = new PreferenceManager(context);
        preferenceManager.setCategoriesArray(categories);
    }

    public static final FirebaseRemoteConfig FIREBASE_REMOTE_CONFIG = FirebaseRemoteConfig.getInstance();

    public static final int EXPIRATION_TIME = 0;

    static{
        FIREBASE_REMOTE_CONFIG.setConfigSettings(new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true).build());
        FIREBASE_REMOTE_CONFIG.setDefaults(R.xml.remote_config_defaults);
    }
}
