package in.blogspot.choosejava.andronews2.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.Arrays;

import in.blogspot.choosejava.andronews2.R;

import static android.app.Activity.RESULT_OK;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final int RC_SIGN_IN = 100;

    private Button loginButton, logoutButton;
    private ImageView profileImageView;
    private TextView welcomeTextView;

    @Override
    public void onResume() {
        super.onResume();
        showUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        initViews(view);

        loginButton.setOnClickListener(this);
        logoutButton.setOnClickListener(this);

        //RecyclerView code
        RecyclerView myRecyclerView = view.findViewById(R.id.fragment_login_recyclerView);
        myRecyclerView.setAdapter(new LoggedInSectionAdapter());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        myRecyclerView.setLayoutManager(layoutManager);
        myRecyclerView.setHasFixedSize(true);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(myRecyclerView.getContext(),
                layoutManager.getOrientation());
        myRecyclerView.addItemDecoration(dividerItemDecoration);

        return view;
    }

    private void initViews(View rootView) {
        loginButton = rootView.findViewById(R.id.fragment_login_loginButton);
        logoutButton = rootView.findViewById(R.id.fragment_login_logoutButton);
        profileImageView = rootView.findViewById(R.id.fragment_login_imageView);
        welcomeTextView = rootView.findViewById(R.id.fragment_login_welcome_textView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            handleSignInResponse(resultCode, data);
            return;
        }
        Toast.makeText(getActivity(), "Unknown error occured", Toast.LENGTH_SHORT).show();
        finishThis();
    }

    private void signIn(){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        //If logged out
        if(auth.getCurrentUser() == null)
            startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setTheme(R.style.AppTheme)
                        .setLogo(R.mipmap.ic_launcher)
                        .setAvailableProviders(Arrays.asList(
                                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()))
                        .setIsSmartLockEnabled(true)
                        .build(),
                RC_SIGN_IN);
    }

    public void signOut() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        //If already logged in
        if(auth.getCurrentUser() != null)
            AuthUI.getInstance()
                .signOut(getActivity())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //Logged out
                            showUserDetails();
                        } else {
                            //Error
                            Toast.makeText(getActivity(), "Error!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @MainThread
    private void handleSignInResponse(int resultCode, Intent data) {
        IdpResponse response = IdpResponse.fromResultIntent(data);

        // Successfully signed in
        if (resultCode == RESULT_OK) {
            //startSignedInActivity(response);
            showUserDetails();
            return;
        } else {
            // Sign in failed
            if (response == null) {
                // User pressed back button
                Toast.makeText(getActivity(), "Sign-in cancelled", Toast.LENGTH_SHORT).show();
                finishThis();
                return;
            }

            if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                finishThis();
                return;
            }

            if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                Toast.makeText(getActivity(), "Unknown error", Toast.LENGTH_SHORT).show();
                finishThis();
                return;
            }
        }

        Toast.makeText(getActivity(), "Unknown sign-in response", Toast.LENGTH_SHORT).show();
        finishThis();
    }

    private void showUserDetails() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = auth.getCurrentUser();
        if(currentUser != null){
            welcomeTextView.setText(currentUser.getDisplayName());

            Picasso.with(getActivity())
                    .load(currentUser.getPhotoUrl())
                    .fit()
                    .centerInside()
                    .into(profileImageView);

            loginButton.setVisibility(View.GONE);
            logoutButton.setVisibility(View.VISIBLE);
        } else{
            welcomeTextView.setText("Welcome");

            Picasso.with(getActivity())
                    .load(R.mipmap.ic_launcher)
                    .fit()
                    .centerInside()
                    .into(profileImageView);

            loginButton.setVisibility(View.VISIBLE);
            logoutButton.setVisibility(View.GONE);
        }

    }

    private void finishThis() {
        //TODO: Show a main section as sign-in was not successful
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.fragment_login_loginButton: {
                signIn();
                break;
            }

            case R.id.fragment_login_logoutButton: {
                signOut();
                break;
            }
        }
    }

    private static class LoggedInSectionAdapter extends RecyclerView.Adapter<LoggedInSectionAdapter.MyViewHolder> {

        private String[] settingsItems = new String[]{
                "",
                "My Messages",
                "Favorites",
                "Feedback",
                "Settings"
        };

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Context context;
            public View mainView;
            public TextView settingsItemTextView;

            public MyViewHolder(View view) {
                super(view);

                mainView = view;

                settingsItemTextView = (TextView) view.findViewById(R.id.login_settings_item_textView);
                context = mainView.getContext();
            }
        }

        @Override
        public LoggedInSectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.login_settings_item, parent, false);

            return new LoggedInSectionAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final LoggedInSectionAdapter.MyViewHolder holder, int position) {
            String categoryText = settingsItems[position];
            holder.settingsItemTextView.setText(categoryText);

            holder.mainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(holder.context, "Coming soon", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return settingsItems.length;
        }
    }
}
