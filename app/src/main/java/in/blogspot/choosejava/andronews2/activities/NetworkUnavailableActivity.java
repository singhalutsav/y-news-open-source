package in.blogspot.choosejava.andronews2.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.Utils;

public class NetworkUnavailableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_unavailable);

        Button retryConnectionButton = (Button) findViewById(R.id.network_unavailable_retry_button);
        retryConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Utils.isNetworkAvailable(NetworkUnavailableActivity.this)){
                    Intent intent = new Intent(NetworkUnavailableActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else{
                    Toast.makeText(NetworkUnavailableActivity.this, "Network unavailable", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Utils.isNetworkAvailable(this)){
            Intent intent = new Intent(NetworkUnavailableActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else{
            Toast.makeText(NetworkUnavailableActivity.this, "Network unavailable", Toast.LENGTH_SHORT).show();
        }
    }
}
