package in.blogspot.choosejava.andronews2.misc;

import in.blogspot.choosejava.andronews2.models.NewsArticleCategoryModel;
import in.blogspot.choosejava.andronews2.models.NewsArticlesModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Admin on 10/22/2017.
 */

public interface NewsApi {
    @GET("/v1/articles")
    Call<NewsArticlesModel> getNewsArticles
            (@Query("source") String source, @Query("apiKey") String apiKey);

    @GET("/v2/top-headlines")
    Call<NewsArticleCategoryModel> getNewsArticles2
            (@Query("category") String category, @Query("country") String country, @Query("apiKey") String apiKey);
}
