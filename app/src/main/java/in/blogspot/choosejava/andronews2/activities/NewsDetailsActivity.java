package in.blogspot.choosejava.andronews2.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import in.blogspot.choosejava.andronews2.R;
import in.blogspot.choosejava.andronews2.misc.Utils;

public class NewsDetailsActivity extends BaseActivity {

    public static final String URL_EXTRA = "url-string";
    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        progressBar = (ProgressBar)findViewById(R.id.home_tab_news_details_progressBar);
        webView = (WebView) findViewById(R.id.home_tab_news_details_webView);

        webView.setWebViewClient(new MyWebClient());
        webView.getSettings().setJavaScriptEnabled(true);

        String url = getIntent().getStringExtra(URL_EXTRA);
        webView.loadUrl(url);
    }

    public class MyWebClient extends WebViewClient{

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
            Utils.startTimer(progressBar);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
