package in.blogspot.choosejava.andronews2.misc;

/**
 * Created by RITU RAJ on 28-05-2017.
 */

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;


public class PreferenceManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "andronews";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    private static final String CATEGORIES_ARRAY = "categoriesArray";

    public PreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setCategoriesArray(String[] categories) {
        editor.putStringSet(CATEGORIES_ARRAY, new LinkedHashSet<>(Arrays.asList(categories)));
        editor.commit();
    }

    public String[] getCategoriesArray() {
        Set<String> set = pref.getStringSet(CATEGORIES_ARRAY, new LinkedHashSet<String>(Arrays.asList(Utils.CATEGORIES)));
        String[] result = set.toArray(new String[set.size()]);
        return result;
    }

}
