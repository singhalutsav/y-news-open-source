# Y News Open Source

[Play Store link](https://play.google.com/store/apps/details?id=in.blogspot.choosejava.andronews2)

Welcome! The UI of this android app is inspired by [UC News](https://play.google.com/store/apps/details?id=com.uc.iflow&hl=en_IN). The news sources are basically for Indian audience but can be edited.

## Install Instructions

- Clone and build the project.
- You will need to add a **secrets.properties** file to the root directory.
	- Obtain api key from  [News API](https://newsapi.org)
	- Obtain api key from [Youtube Developer API](https://developers.google.com/youtube/v3/getting-started)
	- Sample **secrets.properties**(on separate lines):
	NEWS_API_KEY=XYZ123
	YOUTUBE_API_KEY=123890
- Connect android project to [Firebase](https://firebase.google.com). Set up authentication (Facebook and Google) and crashlytics.
- Download **google-services.json**.
- The project should now build and run.

### License:

GNU General Public License V3
Pull requests welcome.
Copyright: Utsav Singhal